﻿// A ComboBox with a TreeView Drop-Down
// Bradley Smith - 2010/11/04 (updated 2016/10/25)

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using static HiVE.CheckBoxControls.CntBoxSpecialMethods;

namespace HiVE.CheckBoxControls
{
    /// <summary>
    /// Represents a control which provides ComboBox-like functionality, displaying its 
    /// dropdown items (nodes) in a manner similar to a TreeView control.
    /// </summary>
    [ToolboxItem(true), DesignerCategory("")]
    public class CheckTreeBox : INotifyPropertyChanged // : System.Windows.Controls.Control/*, System.Windows.Controls.TreeView*/
    {
        #region Feilds

        internal const CntMethodTextFormatFlags TEXT_FORMAT_FLAGS = CntMethodTextFormatFlags.TextBoxControl | CntMethodTextFormatFlags.Left | CntMethodTextFormatFlags.VerticalCenter | CntMethodTextFormatFlags.PathEllipsis;
        internal const string DEFAULT_PATH_SEPARATOR = @"\";
        internal const string DEFAULT_CHECKED_NODE_SEPARATOR = " | ";

        private ComboTreeCheckBox _checkBox;
        private int _expandedImageIndex;
        private string _expandedImageKey;
        private int _imageIndex;
        private string _imageKey;
        private IList<Image> _images;
        private bool _isUpdating;
        private CntBoxTreeNodeCollection _nodes;
        private string _nullValue;
        private string _pathSeparator;
        private string _checkedNodeSeparator;
        private CntBoxTreeNode _selectedNode;
        private bool _showPath;
        private bool _useNodeNamesForPath;
        private bool _showCheckBoxes;
        private bool _threeState;
        private bool _cascadeCheckState;
        private int _recurseDepth;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the (recursive) superset of the entire tree of nodes contained 
        /// within the control.
        /// </summary>
        [Browsable(false)]
        public IEnumerable<CntBoxTreeNode> AllNodes
        {
            get
            {
                IEnumerator<CntBoxTreeNode> e = CntBoxTreeNodeCollection.GetNodesRecursive(_nodes, false);
                while (e.MoveNext()) yield return e.Current;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether the check state of a node is 
        /// determined by its child nodes, and vice versa. If set to true, this 
        /// means that only the check state of leaf nodes is significant.
        /// </summary>
        [DefaultValue(true), Description("Determines whether the check state of a node is determined by its child nodes, and vice versa."), Category("Behavior")]
        public bool CascadeCheckState
        {
            get
            {
                return _cascadeCheckState;
            }
            set
            {
                bool diff = (_cascadeCheckState != value);
                _cascadeCheckState = value;

                if (diff && _cascadeCheckState)
                {
                    // apply cascading state
                    IEnumerator<CntBoxTreeNode> e = CntBoxTreeNodeCollection.GetNodesRecursive(_nodes, true);
                    while (e.MoveNext())
                    {
                        if (e.Current.Nodes.Count > 0) e.Current.CheckState = e.Current.GetAggregateCheckState();
                    }
                }
            }
        }
        /// <summary>
        /// Gets or sets a (recursive) sequence containing the nodes whose 
        /// <see cref="CntBoxTreeNode.CheckState"/> property is equal to 
        /// <see cref="CheckState.Checked"/>. If the <see cref="CascadeCheckState"/> 
        /// property is set to true, only leaf nodes are included.
        /// </summary>
        [Browsable(false)]
        public IEnumerable<CntBoxTreeNode> CheckedNodes
        {
            get
            {
                IEnumerator<CntBoxTreeNode> e = CntBoxTreeNodeCollection.GetNodesRecursive(_nodes, false);
                while (e.MoveNext())
                {
                    if (e.Current.IsChecked)
                    {
                        if (_cascadeCheckState && (e.Current.Nodes.Count > 0)) continue;
                        yield return e.Current;
                    }
                }
            }
            set
            {
                IEnumerator<CntBoxTreeNode> e = CntBoxTreeNodeCollection.GetNodesRecursive(_nodes, false);
                while (e.MoveNext())
                {
                    e.Current.IsChecked = false;
                }
                foreach (CntBoxTreeNode node in value)
                {
                    node.IsChecked = true;
                }
            }
        }
        /// <summary>
        /// Gets or sets the string used to separate the checked nodes.
        /// </summary>
        [DefaultValue(DEFAULT_CHECKED_NODE_SEPARATOR), Description("The string used to separate the checked nodes."), Category("Appearance")]
        public string CheckedNodeSeparator
        {
            get { return _checkedNodeSeparator; }
            set { _checkedNodeSeparator = value; }
        }
        /// <summary>
        /// Gets or sets the maximum height of the dropdown portion of the control.
        /// </summary>
        [DefaultValue(ComboTreeCheckBox.DEFAULT_DROPDOWN_HEIGHT), Description("The maximum height of the dropdown portion of the control."), Category("Behavior")]
        public int DropDownHeight
        {
            get
            {
                return _checkBox.DropDownHeight;
            }
            set
            {
                _checkBox.DropDownHeight = value;
            }
        }
        /// <summary>
        /// Gets or sets the index of the default image to use for nodes when expanded.
        /// </summary>
        [DefaultValue(0), Description("The index of the default image to use for nodes when expanded."), Category("Appearance")]
        public int ExpandedImageIndex
        {
            get { return _expandedImageIndex; }
            set
            {
                _expandedImageIndex = value;
                _checkBox.UpdateVisibleItems();
            }
        }
        /// <summary>
        /// Gets or sets the name of the default image to use for nodes when expanded.
        /// </summary>
        [DefaultValue(""), Description("The name of the default image to use for nodes when expanded."), Category("Appearance")]
        public string ExpandedImageKey
        {
            get { return _expandedImageKey; }
            set
            {
                _expandedImageKey = value;
                _checkBox.UpdateVisibleItems();
            }
        }
        /// <summary>
        /// Gets or sets the index of the default image to use for nodes.
        /// </summary>
        [DefaultValue(0), Description("The index of the default image to use for nodes."), Category("Appearance")]
        public int ImageIndex
        {
            get { return _imageIndex; }
            set
            {
                _imageIndex = value;
                _checkBox.UpdateVisibleItems();
            }
        }
        /// <summary>
        /// Gets or sets the name of the default image to use for nodes.
        /// </summary>
        [DefaultValue(""), Description("The name of the default image to use for nodes."), Category("Appearance")]
        public string ImageKey
        {
            get { return _imageKey; }
            set
            {
                _imageKey = value;
                _checkBox.UpdateVisibleItems();
            }
        }
        /// <summary>
        /// Gets or sets an ImageList component which provides the images displayed beside nodes in the control.
        /// </summary>
        [DefaultValue(null), Description("An ImageList component which provides the images displayed beside nodes in the control."), Category("Appearance")]
        public IList<Image> Images
        {
            get { return _images; }
            set
            {
                _images = value;
                _checkBox.UpdateVisibleItems();
            }
        }
        /// <summary>
        /// Gets or sets the collection of top-level nodes contained by the control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Description("The collection of top-level nodes contained by the control."), Category("Data")]
        public CntBoxTreeNodeCollection Nodes
        {
            get { return _nodes; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");

                if (_nodes != null)
                {
                    _nodes.AfterCheck -= nodes_AfterCheck;
                    _nodes.CollectionChanged -= nodes_CollectionChanged;
                }

                _nodes = value;
                _nodes.CollectionChanged += nodes_CollectionChanged;
                _nodes.AfterCheck += nodes_AfterCheck;
            }
        }
        /// <summary>
        /// Gets or sets the text displayed in the editable portion of the control if the SelectedNode property is null.
        /// </summary>
        [DefaultValue(""), Description("The text displayed in the editable portion of the control if the SelectedNode property is null."), Category("Appearance")]
        public string NullValue
        {
            get { return _nullValue; }
            set
            {
                _nullValue = value;
                //Invalidate();
            }
        }
        /// <summary>
        /// Gets or sets the path to the selected node.
        /// </summary>
        [DefaultValue(""), Description("The path to the selected node."), Category("Behavior")]
        public string Path
        {
            get
            {
                if (_selectedNode != null)
                    return GetFullPath(_selectedNode);
                else
                    return String.Empty;
            }
            set
            {
                SelectedNode = _nodes.ParsePath(value, _pathSeparator, _useNodeNamesForPath);
            }
        }
        /// <summary>
        /// Gets or sets the string used to separate nodes in the Path property.
        /// </summary>
        [DefaultValue(DEFAULT_PATH_SEPARATOR), Description("The string used to separate nodes in the path string."), Category("Behavior")]
        public string PathSeparator
        {
            get { return _pathSeparator; }
            set
            {
                _pathSeparator = value;
                //if (_showPath) Invalidate();
            }
        }
        /// <summary>
        /// Gets or sets the node selected in the control.
        /// </summary>
        [Browsable(false)]
        public CntBoxTreeNode SelectedNode
        {
            get { return _selectedNode; }
            set
            {
                if (!OwnsNode(value)) throw new ArgumentException("Node does not belong to this control.", "value");
                SetSelectedNode(value);
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether a checkbox is shown beside each node.
        /// </summary>
        [DefaultValue(false), Description("Determines whether a checkbox is shown beside each node."), Category("Behavior")]
        public bool ShowCheckBoxes
        {
            get { return _showCheckBoxes; }
            set
            {
                _showCheckBoxes = value;
                _selectedNode = null;
                //Invalidate();
            }
        }
        /// <summary>
        /// Determines whether the full path to the selected node is displayed in the editable portion of the control.
        /// </summary>
        [DefaultValue(false), Description("Determines whether the path to the selected node is displayed in the editable portion of the control."), Category("Appearance")]
        public bool ShowPath
        {
            get { return _showPath; }
            set
            {
                _showPath = value;
                //Invalidate();
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether node checkboxes move into the <see cref="CheckState.Indeterminate"/> state after the <see cref="CheckState.Checked"/> state.
        /// </summary>
        [DefaultValue(false), Description("Determines whether node checkboxes move into the indeterminate/mixed state after the checked state."), Category("Behavior")]
        public bool ThreeState
        {
            get { return _threeState; }
            set { _threeState = value; }
        }
        /// <summary>
        /// Gets or sets the first visible BoxTreeNode in the drop-down portion of the control.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public CntBoxTreeNode FirstVisibleNode
        {
            get
            {
                return _checkBox.TopNode;
            }
            set
            {
                _checkBox.TopNode = value;
            }
        }
        /// <summary>
        /// Determines whether the <see cref="CntBoxTreeNode.Name"/> property of the nodes is used to construct the path string. 
        /// The default behaviour is to use the <see cref="CntBoxTreeNode.Text"/> property.
        /// </summary>
        [DefaultValue(false), Description("Determines whether the Name property of the nodes is used to construct the path string. The default behaviour is to use the Text property."), Category("Behavior")]
        public bool UseNodeNamesForPath
        {
            get { return _useNodeNamesForPath; }
            set
            {
                _useNodeNamesForPath = value;
                //if (_showPath) Invalidate();
            }
        }
        /// <summary>
        /// Gets the number of BoxTreeNodes visible in the drop-down portion of the control.
        /// </summary>
        [Browsable(false)]
        public int VisibleCount
        {
            get
            {
                return _checkBox.VisibleCount;
            }
        }
        /// <summary>
        /// Gets a value indicating whether glyph lines need to be drawn to the left of each node.
        /// </summary>
        internal bool NodeLinesNeeded
        {
            get
            {
                bool childrenFound = false;
                foreach (CntBoxTreeNode node in Nodes)
                {
                    if (node.Nodes.Count > 0)
                    {
                        childrenFound = true;
                        break;
                    }
                }
                return childrenFound;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initalises a new instance of CheckTreeBox.
        /// </summary>
        public CheckTreeBox()
        {
            // default property values
            _nullValue = String.Empty;
            _pathSeparator = DEFAULT_PATH_SEPARATOR;
            _checkedNodeSeparator = DEFAULT_CHECKED_NODE_SEPARATOR;
            _expandedImageIndex = _imageIndex = 0;
            _expandedImageKey = _imageKey = String.Empty;
            _cascadeCheckState = true;

            // nodes collection
            Nodes = new CntBoxTreeNodeCollection(null);

            // dropdown portion
            _checkBox = new ComboTreeCheckBox(this);
            _checkBox.UpdateVisibleItems();
        }

        #endregion

        #region Internal\Private Methods and Events

        /// <summary>
        /// Returns the next displayable node, relative to the selected node.
        /// </summary>
        /// <returns></returns>
        private CntBoxTreeNode GetNextDisplayedNode()
        {
            bool started = false;
            IEnumerator<CntBoxTreeNode> e = CntBoxTreeNodeCollection.GetNodesRecursive(_nodes, false);
            while (e.MoveNext())
            {
                if (started || (_selectedNode == null))
                {
                    if (IsNodeVisible(e.Current)) return e.Current;
                }
                else if (e.Current == _selectedNode)
                {
                    started = true;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the previous displayable node, relative to the selected node.
        /// </summary>
        /// <returns></returns>
        private CntBoxTreeNode GetPrevDisplayedNode()
        {
            bool started = false;
            IEnumerator<CntBoxTreeNode> e = CntBoxTreeNodeCollection.GetNodesRecursive(_nodes, true);
            while (e.MoveNext())
            {
                if (started || (_selectedNode == null))
                {
                    if (IsNodeVisible(e.Current)) return e.Current;
                }
                else if (e.Current == _selectedNode)
                {
                    started = true;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the image referenced by the specified node in the ImageList component associated with this control.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        internal Image GetNodeImage(CntBoxTreeNode node)
        {
            return GetNodeImage(node, _images, _imageIndex, _imageKey, _expandedImageIndex, _expandedImageKey);
        }

        /// <summary>
        /// Returns the image associated with the specified node.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="images"></param>
        /// <param name="imageIndex"></param>
        /// <param name="imageKey"></param>
        /// <param name="expandedImageIndex"></param>
        /// <param name="expandedImageKey"></param>
        /// <returns></returns>
        internal static Image GetNodeImage(CntBoxTreeNode node, IList<Image> images, int imageIndex, string imageKey, int expandedImageIndex, string expandedImageKey)
        {
            if ((images != null) && (node != null))
            {
                if (node.Expanded)
                {
                    //if (images.Images.ContainsKey(node.ExpandedImageKey))
                    //return images.Images[node.ExpandedImageKey];        // node's key
                    /*else */
                    if (node.ExpandedImageIndex >= 0)
                        return images[node.ExpandedImageIndex];      // node's index
                    //else if (images.Images.ContainsKey(expandedImageKey))
                    //    return images.Images[expandedImageKey];             // default key
                    else if ((expandedImageIndex >= 0) && (expandedImageIndex < images.Count))
                        return images[expandedImageIndex];           // default index
                }
                else
                {
                    //if (images.Images.ContainsKey(node.ImageKey))
                    //    return images.Images[node.ImageKey];        // node's key
                    /*else */
                    if (node.ImageIndex >= 0)
                        return images[node.ImageIndex];      // node's index
                    //else if (images.Images.ContainsKey(imageKey))
                    //    return images.Images[imageKey];             // default key
                    else if ((imageIndex >= 0) && (imageIndex < images.Count))
                        return images[imageIndex];           // default index
                }
            }

            return null;
        }

        /// <summary>
        /// Determines whether the specified node should be displayed.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        internal bool IsNodeVisible(CntBoxTreeNode node)
        {
            bool displayed = true;
            CntBoxTreeNode parent = node;
            while ((parent = parent.Parent) != null)
            {
                if (!parent.Expanded)
                {
                    displayed = false;
                    break;
                }
            }
            return displayed;
        }

        /*
        /// <summary>
        /// Scrolls between adjacent nodes, or scrolls the drop-down portion of 
        /// the control in response to the mouse wheel.
        /// </summary>
        /// <param name="e"></param>
        protected void OnMouseWheel(MouseEventArgs e)
        {
            HandledMouseEventArgs he = (HandledMouseEventArgs)e;
            he.Handled = true;

            if (e.Delta > 0)
            {
                BoxTreeNode prev = GetPrevDisplayedNode();
                if (prev != null) SetSelectedNode(prev);
            }
            else if (e.Delta < 0)
            {
                BoxTreeNode next = GetNextDisplayedNode();
                if (next != null) SetSelectedNode(next);
            }
        }
        */

        /*
        /// <summary>
        /// Handles keyboard shortcuts.
        /// </summary>
        /// <param name="e"></param>
        protected void OnKeyDown(KeyEventArgs e)
        {
            e.Handled = e.SuppressKeyPress = true;

            if ((e.KeyCode == Keys.Up) || (e.KeyCode == Keys.Left))
            {
                BoxTreeNode prev = GetPrevDisplayedNode();
                if (prev != null) SetSelectedNode(prev);
            }
            else if ((e.KeyCode == Keys.Down) || (e.KeyCode == Keys.Right))
            {
                BoxTreeNode next = GetNextDisplayedNode();
                if (next != null) SetSelectedNode(next);
            }
            else
            {
                e.Handled = e.SuppressKeyPress = false;
            }
        }
        */

        /// <summary>
        /// Returns a string containing the concatenated text of the checked nodes.
        /// </summary>
        /// <returns></returns>
        private string GetCheckedNodeString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (CntBoxTreeNode node in CheckedNodes)
            {
                if (_showPath)
                    sb.Append(GetFullPath(node));
                else
                    sb.Append(node.Text);

                sb.Append(_checkedNodeSeparator);
            }

            if (sb.Length > 0) sb.Remove(sb.Length - _checkedNodeSeparator.Length, _checkedNodeSeparator.Length);

            return sb.ToString();
        }

        /// <summary>
        /// Raises the <see cref="SelectedNodeChanged"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSelectedNodeChanged(EventArgs e)
        {
            if (SelectedNodeChanged != null) SelectedNodeChanged(this, e);
        }

        /// <summary>
        /// Raises the <see cref="AfterCheck"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected internal virtual void OnAfterCheck(CntBoxTreeNodeEventArgs e)
        {
            if (AfterCheck != null) AfterCheck(this, e);
        }

        /// <summary>
        /// Determines whether the specified node belongs to this CheckTreeBox, and 
        /// hence is a valid selection. For the purposes of this method, a null 
        /// value is always a valid selection.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool OwnsNode(CntBoxTreeNode node)
        {
            if (node == null) return true;

            CntBoxTreeNode parent = node;
            while (parent.Parent != null) parent = parent.Parent;
            return _nodes.Contains(parent);
        }

        /// <summary>
        /// Sets the value of the SelectedNode property and raises the SelectedNodeChanged event.
        /// </summary>
        /// <param name="node"></param>
        private void SetSelectedNode(CntBoxTreeNode node)
        {
            if ((_selectedNode != node) && !_showCheckBoxes)
            {
                _selectedNode = node;
                //Invalidate();
                OnSelectedNodeChanged(EventArgs.Empty);
            }
        }

        private void nodes_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!_isUpdating)
            {
                // verify that selected node still belongs to the tree
                if (!OwnsNode(_selectedNode)) SetSelectedNode(null);

                // rebuild the view
                _checkBox.UpdateVisibleItems();
            }
        }

        private void nodes_AfterCheck(object sender, CntBoxTreeNodeEventArgs e)
        {
            if (_cascadeCheckState)
            {
                _recurseDepth++;

                if (_recurseDepth == 1)
                {
                    IEnumerator<CntBoxTreeNode> enumerator = CntBoxTreeNodeCollection.GetNodesRecursive(e.Node.Nodes, false);
                    while (enumerator.MoveNext())
                    {
                        if (_threeState)
                            enumerator.Current.CheckState = e.Node.CheckState;
                        else
                            enumerator.Current.IsChecked = e.Node.IsChecked;
                    }

                    CntBoxTreeNode parent = e.Node.Parent;
                    while (parent != null)
                    {
                        parent.CheckState = parent.GetAggregateCheckState();
                        parent = parent.Parent;
                    }
                }

                _recurseDepth--;
            }

            //Invalidate();
            OnAfterCheck(e);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Fired when the value of the <see cref="SelectedNode"/> property changes.
        /// </summary>
        [Description("Occurs when the SelectedNode property changes.")]
        public event EventHandler SelectedNodeChanged;

        /// <summary>
        /// Fired when the value of a node's <see cref="CntBoxTreeNode.CheckState"/> property changes.
        /// </summary>
        [Description("Occurs when a node checkbox is checked.")]
        public event EventHandler<CntBoxTreeNodeEventArgs> AfterCheck;

        /// <summary>
        /// Prevents the dropdown portion of the control from being updated until the EndUpdate method is called.
        /// </summary>
        public void BeginUpdate()
        {
            _isUpdating = true;
        }

        /// <summary>
        /// Collapses all nodes in the tree for when the dropdown portion of the control is reopened.
        /// </summary>
        public void CollapseAll()
        {
            foreach (CntBoxTreeNode node in AllNodes) node.Expanded = false;
        }

        /*
        /// <summary>
        /// Disposes of the control and its dropdown.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) _checkBox.Dispose();
            base.Dispose(disposing);
        }
        */

        /// <summary>
        /// Updates the dropdown portion of the control after being suspended by the BeginUpdate method.
        /// </summary>
        public void EndUpdate()
        {
            _isUpdating = false;
            if (!OwnsNode(_selectedNode)) SetSelectedNode(null);
            _checkBox.UpdateVisibleItems();
        }

        /// <summary>
        /// Expands all nodes in the tree for when the dropdown portion of the control is reopened.
        /// </summary>
        public void ExpandAll()
        {
            foreach (CntBoxTreeNode node in AllNodes) if (node.Nodes.Count > 0) node.Expanded = true;
        }

        /// <summary>
        /// Returns the full path to the specified <see cref="BoxTreeNode"/>.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public string GetFullPath(CntBoxTreeNode node)
        {
            if (node == null) throw new ArgumentNullException("node");

            return node.GetFullPath(_pathSeparator, _useNodeNamesForPath);
        }

        /// <summary>
        /// Returns the node at the specified path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public CntBoxTreeNode GetNodeAt(string path)
        {
            return _nodes.ParsePath(path, _pathSeparator, _useNodeNamesForPath);
        }

        /// <summary>
        /// Sorts the contents of the tree using the default comparer.
        /// </summary>
        public void Sort()
        {
            Sort(null);
        }

        /// <summary>
        /// Sorts the contents of the tree using the specified comparer.
        /// </summary>
        /// <param name="comparer"></param>
        public void Sort(IComparer<CntBoxTreeNode> comparer)
        {
            bool oldIsUpdating = _isUpdating;
            _isUpdating = true;
            _nodes.Sort(comparer);
            if (!oldIsUpdating) EndUpdate();
        }

        public void Initialize()
        {
            foreach (CntBoxTreeNode node in this.Nodes)
            {
                node.Parent = _selectedNode;
                node.Initialize();
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        void OnPropertyChanged(string prop)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}