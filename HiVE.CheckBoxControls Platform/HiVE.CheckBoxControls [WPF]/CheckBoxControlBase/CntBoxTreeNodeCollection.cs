﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using static HiVE.CheckBoxControls.CntBoxSpecialMethods;

namespace HiVE.CheckBoxControls
{
    /// <summary>
    /// Represents a collection of BoxTreeNode objects contained within a node or a BcBoxTreeNode control. 
    /// Supports change notification through INotifyCollectionChanged. Implements the non-generic IList to 
    /// provide design-time support.
    /// </summary>  
    public class CntBoxTreeNodeCollection : IList<CntBoxTreeNode>, IList, INotifyCollectionChanged
    {
        #region Feilds

        private List<CntBoxTreeNode> _innerList;
        private CntBoxTreeNode _node;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialises a new instance of CntBoxTreeNodeCollection using default (empty) values.
        /// </summary>
        public CntBoxTreeNodeCollection()
        {
            _innerList = new List<CntBoxTreeNode>();
            _node = new CntBoxTreeNode();
        }

        /// <summary>
        /// Initalises a new instance of BoxTreeNodeCollection and associates it with the specified BoxTreeNode.
        /// </summary>
        /// <param name="node"></param>
        public CntBoxTreeNodeCollection(CntBoxTreeNode node)
        {
            _innerList = new List<CntBoxTreeNode>();
            _node = node;
        }

        #endregion

        #region Internal\Private Methods and Events

        internal static IEnumerator<CntBoxTreeNode> GetNodesRecursive(CntBoxTreeNodeCollection collection, bool reverse)
        {
            if (!reverse)
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    yield return collection[i];
                    IEnumerator<CntBoxTreeNode> e = GetNodesRecursive(collection[i].Nodes, reverse);
                    while (e.MoveNext()) yield return e.Current;
                }
            }
            else
            {
                for (int i = (collection.Count - 1); i >= 0; i--)
                {
                    IEnumerator<CntBoxTreeNode> e = GetNodesRecursive(collection[i].Nodes, reverse);
                    while (e.MoveNext()) yield return e.Current;
                    yield return collection[i];
                }
            }
        }

        /// <summary>
        /// Fired when the check state of a node in the collection (or one of its children) changes.
        /// </summary>
        [Browsable(false)]
        internal event EventHandler<CntBoxTreeNodeEventArgs> AfterCheck;

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null) CollectionChanged(this, e);
        }

        /// <summary>
        /// Sorts the collection and its entire sub-tree using the specified comparer.
        /// </summary>
        /// <param name="comparer"></param>
        internal void Sort(IComparer<CntBoxTreeNode> comparer)
        {
            if (comparer == null) comparer = Comparer<CntBoxTreeNode>.Default;
            SortInternal(comparer);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Recursive helper method for Sort(IComparer&lt;BoxTreeNode&gt;).
        /// </summary>
        /// <param name="comparer"></param>
        private void SortInternal(IComparer<CntBoxTreeNode> comparer)
        {
            _innerList.Sort(comparer);
            foreach (CntBoxTreeNode node in _innerList)
            {
                node.Nodes.Sort(comparer);
            }
        }

        /// <summary>
        /// Adds event handlers to the specified node.
        /// </summary>
        /// <param name="item"></param>
        private void AddEventHandlers(CntBoxTreeNode item)
        {
            item.CheckStateChanged += item_CheckStateChanged;
            item.Nodes.CollectionChanged += CollectionChanged;
            item.Nodes.AfterCheck += AfterCheck;
        }

        /// <summary>
        /// Removes event handlers from the specified node.
        /// </summary>
        /// <param name="item"></param>
        private void RemoveEventHandlers(CntBoxTreeNode item)
        {
            item.CheckStateChanged -= item_CheckStateChanged;
            item.Nodes.CollectionChanged -= CollectionChanged;
            item.Nodes.AfterCheck -= AfterCheck;
        }

        /// <summary>
        /// Raises the <see cref="AfterCheck"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnAfterCheck(CntBoxTreeNodeEventArgs e)
        {
            if (AfterCheck != null) AfterCheck(this, e);
        }

        /// <summary>
        /// Returns the <see cref="CntBoxTreeNode"/> that corresponds to the specified path string.
        /// </summary>
        /// <param name="path">The path string.</param>
        /// <param name="pathSeparator">The path separator.</param>
        /// <param name="useNodeNamesForPath">Whether the path is constructed from the name of the node instead of its text.</param>
        /// <returns>The node, or null if the path is empty.</returns>
        internal CntBoxTreeNode ParsePath(string path, string pathSeparator, bool useNodeNamesForPath)
        {
            CntBoxTreeNode select = null;

            string[] parts = path.Split(new string[] { pathSeparator }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < parts.Length; i++)
            {
                CntBoxTreeNodeCollection collection = ((select == null) ? this : select.Nodes);
                if (useNodeNamesForPath)
                {
                    try
                    {
                        select = collection[parts[i]];
                    }
                    catch (KeyNotFoundException ex)
                    {
                        throw new ArgumentException("Invalid path string.", "value", ex);
                    }
                }
                else
                {
                    bool found = false;
                    foreach (CntBoxTreeNode node in collection)
                    {
                        if (node.Text.Equals(parts[i], StringComparison.InvariantCultureIgnoreCase))
                        {
                            select = node;
                            found = true;
                            break;
                        }
                    }
                    if (!found) throw new ArgumentException("Invalid path string.", "value");
                }
            }

            return select;
        }

        private void item_CheckStateChanged(object sender, EventArgs e)
        {
            OnAfterCheck(new CntBoxTreeNodeEventArgs(sender as CntBoxTreeNode));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the node with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public CntBoxTreeNode this[string name]
        {
            get
            {
                foreach (CntBoxTreeNode o in this)
                {
                    if (Object.Equals(o.Name, name)) return o;
                }

                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Creates a node and adds it to the collection.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public CntBoxTreeNode Add(string text)
        {
            CntBoxTreeNode item = new CntBoxTreeNode(text);
            Add(item);
            return item;
        }

        /// <summary>
        /// Creates a node and adds it to the collection.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public CntBoxTreeNode Add(string name, string text)
        {
            CntBoxTreeNode item = new CntBoxTreeNode(name, text);
            Add(item);
            return item;
        }

        /// <summary>
        /// Adds a range of BoxTreeNode to the collection.
        /// </summary>
        /// <param name="items"></param>
        public void AddRange(IEnumerable<CntBoxTreeNode> items)
        {
            foreach (CntBoxTreeNode item in items)
            {
                _innerList.Add(item);
                item.Parent = _node;
                AddEventHandlers(item);
            }
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Adds a range of BoxTreeNode to the collection.
        /// Return the current collection node.
        /// </summary>
        /// <param name="items"></param>
        public IEnumerable<CntBoxTreeNode> AddRangeNode(IEnumerable<CntBoxTreeNode> items)
        {
            foreach (CntBoxTreeNode item in items)
            {
                _innerList.Add(item);
                item.Parent = _node;
                AddEventHandlers(item);
            }
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            return items;
        }

        /// <summary>
        /// Determines whether the collection contains a node with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ContainsKey(string name)
        {
            foreach (CntBoxTreeNode o in this)
            {
                if (Object.Equals(o.Name, name)) return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the node with the specified name from the collection.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Remove(string name)
        {
            for (int i = 0; i < _innerList.Count; i++)
            {
                if (Object.Equals(_innerList[i].Name, name))
                {
                    CntBoxTreeNode item = _innerList[i];
                    RemoveEventHandlers(item);
                    _innerList.RemoveAt(i);
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the index of the node with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int IndexOf(string name)
        {
            for (int i = 0; i < _innerList.Count; i++)
            {
                if (Object.Equals(_innerList[i].Name, name)) return i;
            }

            return -1;
        }

        /// <summary>
        /// Returns the <see cref="CntBoxTreeNode"/> with the specified node text.
        /// </summary>
        /// <param name="text">The text to match.</param>
        /// <param name="comparisonType">The type of string comparison performed.</param>
        /// <param name="recurse">Whether to search recursively through all child nodes.</param>
        /// <returns></returns>
        public CntBoxTreeNode Find(string text, StringComparison comparisonType, bool recurse)
        {
            IEnumerator<CntBoxTreeNode> nodes = recurse ? GetNodesRecursive(this, false) : GetEnumerator();

            while (nodes.MoveNext())
            {
                if (nodes.Current.Text.Equals(text, comparisonType))
                {
                    return nodes.Current;
                }
            }

            return null;
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Returns the aggregate check state of this node's children.
        /// </summary>
        /// <param name="Nodes"></param>
        /// <returns></returns>
        public static bool? GetAggregateIsCheck(CntBoxTreeNodeCollection Nodes)
        {
            bool? state = false;

            bool all = true;
            bool any = false;
            bool chk = false;

            foreach (CntBoxTreeNode child in Nodes)
            {
                if (child.IsChecked != false) any = true;
                if (child.IsChecked != true) all = false;
                if (child.IsChecked == true) chk = true;
            }

            if (all & chk)
                state = true;
            else if (any)
                state = null;

            return state;
        }

        /// <summary>
        /// Returns the aggregate check state of this node's children.
        /// </summary>
        /// <param name="Nodes"></param>
        /// <returns></returns>
        public static CntMethodCheckState GetAggregateCheckState(CntBoxTreeNodeCollection Nodes)
        {
            CntMethodCheckState state = CntMethodCheckState.Unchecked;
            bool all = true;
            bool any = false;
            bool chk = false;

            foreach (CntBoxTreeNode child in Nodes)
            {
                if (child.CheckState != CntMethodCheckState.Unchecked) any = true;
                if (child.CheckState != CntMethodCheckState.Checked) all = false;
                if (child.CheckState == CntMethodCheckState.Checked) chk = true;
            }

            if (all & chk)
                state = CntMethodCheckState.Checked;
            else if (any)
                state = CntMethodCheckState.Indeterminate;

            return state;
        }

        #endregion

        #region ICollection<BoxTreeNode> Members

        /// <summary>
        /// Adds a node to the collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(CntBoxTreeNode item)
        {
            _innerList.Add(item);
            item.Parent = _node;
            AddEventHandlers(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Adds a node to the collection.
        /// Return the current node.
        /// </summary>
        /// <param name="item"></param>
        public CntBoxTreeNode AddNode(CntBoxTreeNode item)
        {
            _innerList.Add(item);
            item.Parent = _node;
            AddEventHandlers(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            return item;
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            foreach (CntBoxTreeNode item in _innerList)
            {
                RemoveEventHandlers(item);
            }
            _innerList.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Determines whether the collection contains the specified node.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(CntBoxTreeNode item)
        {
            return _innerList.Contains(item);
        }

        /// <summary>
        /// Gets the number of nodes in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return _innerList.Count;
            }
        }

        /// <summary>
        /// Copies all the nodes from the collection to a compatible array.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(CntBoxTreeNode[] array, int arrayIndex)
        {
            _innerList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Removes the specified node from the collection.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(CntBoxTreeNode item)
        {
            if (_innerList.Remove(item))
            {
                RemoveEventHandlers(item);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                return true;
            }

            return false;
        }

        bool ICollection<CntBoxTreeNode>.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region IEnumerable<BoxTreeNode> Members

        /// <summary>
        /// Returns an enumerator which can be used to cycle through the nodes in the collection (non-recursive).
        /// </summary>
        /// <returns></returns>
        public IEnumerator<CntBoxTreeNode> GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        #endregion

        #region IList<BoxTreeNode> Members

        /// <summary>
        /// Gets or sets the node at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public CntBoxTreeNode this[int index]
        {
            get
            {
                return _innerList[index];
            }
            set
            {
                CntBoxTreeNode oldItem = _innerList[index];
                _innerList[index] = value;
                value.Parent = _node;
                AddEventHandlers(value);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, oldItem));
            }
        }

        /// <summary>
        /// Returns the index of the specified node.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(CntBoxTreeNode item)
        {
            return _innerList.IndexOf(item);
        }

        /// <summary>
        /// Inserts a node into the collection at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, CntBoxTreeNode item)
        {
            _innerList.Insert(index, item);
            item.Parent = _node;
            AddEventHandlers(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Removes the node at the specified index from the collection.
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            CntBoxTreeNode item = _innerList[index];
            RemoveEventHandlers(item);
            _innerList.RemoveAt(index);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
        }

        #endregion

        #region IEnumerable Members (implemented explicitly)

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        #endregion

        #region IList Members (implemented explicitly)

        int IList.Add(object value)
        {
            Add((CntBoxTreeNode)value);
            return Count - 1;
        }

        bool IList.Contains(object value)
        {
            return Contains((CntBoxTreeNode)value);
        }

        int IList.IndexOf(object value)
        {
            return IndexOf((CntBoxTreeNode)value);
        }

        void IList.Insert(int index, object value)
        {
            Insert(index, (CntBoxTreeNode)value);
        }

        bool System.Collections.IList.IsFixedSize
        {
            get
            {
                return false;
            }
        }

        bool System.Collections.IList.IsReadOnly
        {
            get
            {
                return false;
            }
        }

        void IList.Remove(object value)
        {
            Remove((CntBoxTreeNode)value);
        }

        object System.Collections.IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = (CntBoxTreeNode)value;
            }
        }

        #endregion

        #region ICollection Members (implemented explicitly)

        void ICollection.CopyTo(Array array, int index)
        {
            ((ICollection)_innerList).CopyTo(array, index);
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                return ((ICollection)_innerList).IsSynchronized;
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                return ((ICollection)_innerList).SyncRoot;
            }
        }

        #endregion

        #region INotifyCollectionChanged Members

        /// <summary>
        /// Fired when the collection (sub-tree) changes.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion
    }
}