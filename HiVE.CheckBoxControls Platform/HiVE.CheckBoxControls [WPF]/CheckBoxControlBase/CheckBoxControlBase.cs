﻿// A ComboBox with a TreeView Drop-Down
// Bradley Smith - 2010/11/04 (updated 2016/07/08)

using HiVE.CheckBoxControls.BufferedPainting;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using static HiVE.CheckBoxControls.CntBoxSpecialMethods;

namespace HiVE.CheckBoxControls
{
    /// <summary>
    /// Abstract base class for a control which behaves like a dropdown but does not contain 
    /// logic for displaying a popup window.
    /// </summary>
    [Designer("System.Windows.Forms.Design.UpDownBaseDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), ToolboxItem(false), DesignerCategory("")]
    public abstract class CheckBoxControlBase : Control
    {
        #region Feilds

        const int CONTROL_HEIGHT = 7;

        private bool _drawWithVisualStyles;
        private BufferedPainter<CntMethodComboBoxState> _bufferedPainter;

        #endregion

        #region Properties

        /// <summary>
        /// Determines whether to draw the control with visual styles.
        /// </summary>
        [DefaultValue(true), Description("Determines whether to draw the control with visual styles."), Category("Appearance")]
        public bool DrawWithVisualStyles
        {
            get { return _drawWithVisualStyles; }
            set
            {
                _drawWithVisualStyles = value;
                Invalidate();
            }
        }
        /// <summary>
        /// Gets or sets the background color to use for this control.
        /// </summary>
        [DefaultValue(typeof(Color), "Window")]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }
        /// <summary>
        /// Hides the BackgroundImage property on the designer.
        /// </summary>
        [Browsable(false)]
        public override Image BackgroundImage
        {
            get
            {
                return base.BackgroundImage;
            }
            set
            {
                base.BackgroundImage = value;
            }
        }
        /// <summary>
        /// Hides the BackgroundImageLayout property on the designer.
        /// </summary>
        [Browsable(false)]
        public override ImageLayout BackgroundImageLayout
        {
            get
            {
                return base.BackgroundImageLayout;
            }
            set
            {
                base.BackgroundImageLayout = value;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of CheckBoxControlBase.
        /// </summary>
        public CheckBoxControlBase()
        {
            // control styles
            DoubleBuffered = true;
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.Opaque, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, false);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.Selectable, true);
            SetStyle(ControlStyles.StandardClick, true);
            SetStyle(ControlStyles.UserPaint, true);

            // default values
            _drawWithVisualStyles = Application.RenderWithVisualStyles;
            BackColor = SystemColors.Window;

            // buffered painting
            _bufferedPainter = new BufferedPainter<CntMethodComboBoxState>(this);
            _bufferedPainter.PaintVisualState += new EventHandler<BufferedPaintEventArgs<CntMethodComboBoxState>>(bufferedPainter_PaintVisualState);
            _bufferedPainter.State = _bufferedPainter.DefaultState = CntMethodComboBoxState.Normal;
            _bufferedPainter.AddTransition(CntMethodComboBoxState.Normal, CntMethodComboBoxState.Hot, 250);
            _bufferedPainter.AddTransition(CntMethodComboBoxState.Hot, CntMethodComboBoxState.Normal, 350);
            _bufferedPainter.AddTransition(CntMethodComboBoxState.Pressed, CntMethodComboBoxState.Normal, 350);
        }

        #endregion

        #region Internal\Private Methods and Events

        /// <summary>
        /// Converts a MethodComboBoxState value into its equivalent PushButtonState value.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        private PushButtonState GetPushButtonState(CntMethodComboBoxState state)
        {
            switch (state)
            {
                case CntMethodComboBoxState.Disabled:
                    return PushButtonState.Disabled;
                case CntMethodComboBoxState.Hot:
                    return PushButtonState.Hot;
                case CntMethodComboBoxState.Pressed:
                    return PushButtonState.Pressed;
            }

            return PushButtonState.Normal;
        }

        /// <summary>
        /// Determines the state in which to render the textbox portion of the control (when using visual styles).
        /// </summary>
        /// <returns></returns>
        private CntMethodComboBoxState GetTextBoxState()
        {
            if (!Enabled)
                return CntMethodComboBoxState.Disabled;
            else if (Focused || ClientRectangle.Contains(PointToClient(Cursor.Position)))
                return CntMethodComboBoxState.Hot;
            else
                return CntMethodComboBoxState.Normal;
        }

        /// <summary>
        /// Determines the state in which to render the checkbox button portion of the control (when not using visual styles).
        /// </summary>
        /// <returns></returns>
        private ButtonState GetPlainButtonState()
        {
            if (!Enabled)
                return ButtonState.Inactive;
            else if ((MouseButtons & MouseButtons.Left) == MouseButtons.Left)
                return ButtonState.Pushed;
            else
                return ButtonState.Normal;
        }

        /// <summary>
        /// Registers the arrow keys as input keys.
        /// </summary>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                case Keys.PageDown:
                case Keys.PageUp:
                case Keys.Home:
                case Keys.End:
                case Keys.Enter:
                    return true;
                default:
                    return base.IsInputKey(keyData);
            }
        }

        /// <summary>
        /// Recalculates the fixed height of the control when the font changes.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
            SetHeight();
        }

        /// <summary>
        /// Repaints the focus rectangle when focus changes.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            if (ShowFocusCues) Invalidate();
        }

        /// <summary>
        /// Repaints the focus rectangle when focus changes.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            if (ShowFocusCues) Invalidate();
        }

        /// <summary>
        /// Prevents the control's background from painting normally.
        /// </summary>
        /// <param name="pevent"></param>
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            if (!_drawWithVisualStyles || !_bufferedPainter.BufferedPaintSupported || !_bufferedPainter.Enabled)
            {
                base.OnPaintBackground(pevent);
            }
        }

        /// <summary>
        /// Paints the content in the editable portion of the control, providing additional measurements and operations.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPaintContent(CheckBoxPaintEventArgs e)
        {
            if (PaintContent != null) PaintContent(this, e);
        }

        /// <summary>
        /// Sets the fixed height of the control, based on the font size.
        /// </summary>
        private void SetHeight()
        {
            Height = CONTROL_HEIGHT + Font.Height;
        }

        /// <summary>
        /// Draws a legacy style combo box control.
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="bounds"></param>
        /// <param name="buttonBounds"></param>
        /// <param name="backColor"></param>
        /// <param name="state"></param>
        internal static void DrawLegacyComboBox(Graphics graphics, Rectangle bounds, Rectangle buttonBounds, Color backColor, ButtonState state)
        {
            Rectangle borderRect = bounds;
            borderRect.Height++;
            graphics.FillRectangle(new SolidBrush(backColor), bounds);
            ControlPaint.DrawBorder3D(graphics, borderRect, Border3DStyle.Sunken);
            Rectangle buttonRect = buttonBounds;
            buttonRect.X -= 2;
            buttonRect.Y += 2;
            buttonRect.Height -= 3;
            ControlPaint.DrawComboButton(graphics, buttonRect, state);
        }

        /// <summary>
        /// Paints the control using the Buffered Paint API.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bufferedPainter_PaintVisualState(object sender, BufferedPaintEventArgs<CntMethodComboBoxState> e)
        {
            if (_drawWithVisualStyles && _bufferedPainter.BufferedPaintSupported && _bufferedPainter.Enabled)
            {
                // draw in the vista/win7 style
                VisualStyleRenderer r = new VisualStyleRenderer(VisualStyleElement.Button.PushButton.Normal);
                r.DrawParentBackground(e.Graphics, ClientRectangle, this);

                Rectangle buttonBounds = ClientRectangle;
                buttonBounds.Inflate(1, 1);
                ButtonRenderer.DrawButton(e.Graphics, buttonBounds, GetPushButtonState(e.State));

                //Rectangle clipBounds = _dropDownButtonBounds;
                //clipBounds.Inflate(-2, -2);
                //e.Graphics.SetClip(clipBounds);
                //ComboBoxRenderer.DrawDropDownButton(e.Graphics, _dropDownButtonBounds, e.State);
                e.Graphics.SetClip(ClientRectangle);
            }
            else if (_drawWithVisualStyles && ComboBoxRenderer.IsSupported)
            {
                // draw using the visual style renderer
                //ComboBoxRenderer.DrawTextBox(e.Graphics, ClientRectangle, GetTextBoxState());
                //ComboBoxRenderer.DrawDropDownButton(e.Graphics, _dropDownButtonBounds, e.State);
            }
            else
            {
                // draw using the legacy technique
                //DrawLegacyComboBox(e.Graphics, ClientRectangle, _dropDownButtonBounds, BackColor, GetPlainButtonState());
            }

            //OnPaintContent(new CheckBoxPaintEventArgs(e.Graphics, ClientRectangle, GetTextBoxBounds()));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Fired when the content of the editable portion of the control is painted.
        /// </summary>
        [Description("Occurs when the content of the editable portion of the control is painted.")]
        public event EventHandler<CheckBoxPaintEventArgs> PaintContent;

        #endregion
    }

    /// <summary>
    /// EventArgs class for the 
    /// </summary>
    public class CheckBoxPaintEventArgs : PaintEventArgs
    {

        /// <summary>
        /// Gets the display rectangle for the editable portion of the control.
        /// </summary>
        public Rectangle Bounds
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates a new instance of the DropDownPaintEventArgs class.
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="clipRect"></param>
        /// <param name="bounds"></param>
        public CheckBoxPaintEventArgs(Graphics graphics, Rectangle clipRect, Rectangle bounds) : base(graphics, clipRect)
        {
            Bounds = bounds;
        }

        /// <summary>
        /// Draws a focus rectangle on the editable portion of the control.
        /// </summary>
        public void DrawFocusRectangle()
        {
            Rectangle focus = Bounds;
            focus.Inflate(-3, -3);
            //focus.Width++;
            ControlPaint.DrawFocusRectangle(Graphics, focus);
        }
    }
}