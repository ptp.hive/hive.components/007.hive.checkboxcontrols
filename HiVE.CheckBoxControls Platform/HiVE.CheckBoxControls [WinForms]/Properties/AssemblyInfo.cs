﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Global Common CheckBoxControls for the HiVE Productions [WinForms].")]
[assembly: AssemblyDescription("A WinForms CheckBoxControls component which is part of the TreeViewWithCheckBoxes of Author: Josh Smith Library and DropDownControls of Author: Bradley Smith Library.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HiVE Productions")]
[assembly: AssemblyProduct("HiVE.CheckBoxControls [WinForms]")]
[assembly: AssemblyCopyright("Copyright ©  2017 HiVE Productions Inc. All Rights Reserved.")]
[assembly: AssemblyTrademark("HiVE")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("cccc7c95-955f-442b-8f1f-48150ea58696")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.1705.0613")]
[assembly: AssemblyFileVersion("1.1.1705.0613")]
[assembly: NeutralResourcesLanguage("en")]

