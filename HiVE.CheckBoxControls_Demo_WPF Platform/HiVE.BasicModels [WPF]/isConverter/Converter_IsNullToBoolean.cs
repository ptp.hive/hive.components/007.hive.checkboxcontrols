﻿using System;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_IsNullToBoolean : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool returnValue = true;

            try
            {
                if (value != null)
                {
                    { returnValue = false; }
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Boolean back to IsNull");
        }
    }
}
