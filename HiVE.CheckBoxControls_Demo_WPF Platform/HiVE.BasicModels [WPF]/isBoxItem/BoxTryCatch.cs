﻿using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.BasicModels.isBoxItem
{
    public class BoxTryCatch
    {
        public TcMethodAssemblyProduct TryCatchMethodAssemblyProduct { get; set; }
        public TcMethodTypeTemplate TryCatchMethodTypeTemplate { get; set; }
        public string TryCatchMethodTypeTemplate_Template { get; set; }

        public string TryCatchMethodClassFunctions_Function { get; set; }
        public TcMethodFollowingFunction TryCatchMethodFollowingFunction { get; set; }

        public string MessageError { get; set; }
        public string FriendlyMessageError { get; set; }

        public string FinalMessageError { get; set; }
        public string FinalFriendlyMessageError { get; set; }

        public bool IsHaveError { get; set; }
        public bool IsShowFriendlyMessage { get; set; }
        public bool IsShowFinalMessageError { get; set; }

        public string MsgBoxCulture { get; set; }

        public BoxTryCatch() { }

        public BoxTryCatch(
            TcMethodAssemblyProduct TryCatchMethodAssemblyProduct,
            TcMethodTypeTemplate TryCatchMethodTypeTemplate,
            string stringTryCatchMethodTypeTemplate_AssemblyProduct_TypeTemplate,

            string stringTryCatchBoxClassFunctions_AssemblyProduct_TypeTemplate_ClassFunctions,
            TcMethodFollowingFunction TryCatchMethodFollowingFunction,

            string messageError,
            string friendlyMessageError,

            string finalMessageError,
            string finalFriendlyMessageError,

            bool isHaveError,
            bool isShowFriendlyMessage,
            bool isShowFinalMessageError,

            string msgBoxCulture)
        {
            this.TryCatchMethodAssemblyProduct = TryCatchMethodAssemblyProduct;
            this.TryCatchMethodTypeTemplate = TryCatchMethodTypeTemplate;
            this.TryCatchMethodTypeTemplate_Template = stringTryCatchMethodTypeTemplate_AssemblyProduct_TypeTemplate;

            this.TryCatchMethodClassFunctions_Function = stringTryCatchBoxClassFunctions_AssemblyProduct_TypeTemplate_ClassFunctions;
            this.TryCatchMethodFollowingFunction = TryCatchMethodFollowingFunction;

            this.MessageError = messageError;
            this.FriendlyMessageError = friendlyMessageError;

            this.FinalMessageError = finalMessageError;
            this.FinalFriendlyMessageError = finalFriendlyMessageError;

            this.IsHaveError = isHaveError;
            this.IsShowFriendlyMessage = isShowFriendlyMessage;
            this.IsShowFinalMessageError = isShowFinalMessageError;

            this.MsgBoxCulture = msgBoxCulture;
        }
    }
}
