﻿using HiVE.BasicModels.isBoxItem;
using System.Collections.ObjectModel;
using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;

namespace HiVE.BasicModels.isClass
{
    public class ColorList : ObservableCollection<BoxColorDetails>
    {
        public ColorList()
        {
            try
            {
                /// Get a list of color names.
                var results = EnumExtensions.GetEnumValues(new MethodColors());
                /// Add each one to this collection:
                foreach (var color in results)
                {
                    this.Add(
                        new BoxColorDetails(
                            color.ToString(),
                            color.ToDescriptionString()));
                }
            }
            catch { }
        }
    }
}
