﻿using HiVE.CheckBoxControls;

namespace HiVE.CheckBoxControls_Demo_WPF.Models
{
    internal static class SpecialModels
    {
        /// <summary>
        /// Get all root nodes;
        /// Recursive method to change IsChecked and call for child nodes of each node;
        /// </summary>
        /// <param name="CntBoxTreeNodeItems"></param>
        /// <param name="isChecked"></param>
        /// <returns></returns>
        public static CntBoxTreeNodeCollection BindCntBoxTreeNodeCollection_IsChecked(
            CntBoxTreeNodeCollection CntBoxTreeNodeItems,
            bool isChecked)
        {
            try
            {
                /// Add all root nodes to ComboTreeNode and call for child nodes;
                /// Recursion base case; if given node has no child nodes no more action is taken;
                if (CntBoxTreeNodeItems.Count == 0) { return CntBoxTreeNodeItems; }

                foreach (CntBoxTreeNode item in CntBoxTreeNodeItems)
                {
                    item.IsChecked = isChecked;

                    BindCntBoxTreeNodeCollection_IsChecked(
                        item.Nodes,
                        isChecked);
                }
            }
            catch { }

            return CntBoxTreeNodeItems;
        }
    }
}
