﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using HiVE.CheckBoxControls;
using HiVE.CheckBoxControls_Demo_WPF.Models;
using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using static HiVE.BasicModels.isBoxItem.BoxSpecialMethods;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.CheckBoxControls_Demo_WPF.isUserControl
{
    /// <summary>
    /// Interaction logic for CheckTreeViewBoxUC.xaml
    /// </summary>
    internal partial class CheckTreeViewBoxUC : UserControl
    {
        public CheckTreeViewBoxUC()
        {
            FooBoxTreeNodeItems = AddNodeCollection_FooBoxTreeNode();
            CntBoxTreeNodeItems = AddNodeCollection_CntBoxTreeNode();
            CheckTreeBoxItems = AddNodeCollection_CheckTreeBox();

            InitializeComponent();

            base.CommandBindings.Add(
                new CommandBinding(
                    ApplicationCommands.SelectAll,
                    (sender, e) => // Execute
                    {
                        e.Handled = true;
                        foreach (FooBoxTreeNode item in FooBoxTreeNodeItems)
                        {
                            item.IsChecked = true;
                        }
                        this.treeViewCheckFooBoxTreeNode.Focus();
                    },
                    (sender, e) => // CanExecute
                    {
                        e.Handled = true;
                        e.CanExecute = (FooBoxTreeNodeCollection.GetAggregateIsCheck(FooBoxTreeNodeItems) != true);
                    }));

            base.CommandBindings.Add(
                new CommandBinding(
                    ApplicationCommands.Undo,
                    (sender, e) => // Execute
                    {
                        e.Handled = true;
                        foreach (FooBoxTreeNode item in FooBoxTreeNodeItems)
                        {
                            item.IsChecked = false;
                        }
                        this.treeViewCheckFooBoxTreeNode.Focus();
                    },
                    (sender, e) => // CanExecute
                    {
                        e.Handled = true;
                        e.CanExecute = (FooBoxTreeNodeCollection.GetAggregateIsCheck(FooBoxTreeNodeItems) != false);
                    }));

            this.treeViewCheckFooBoxTreeNode.Focus();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_CheckBoxControls_Demo_WPF;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_CheckBoxControls_Demo_WPF_IsUserControl.CheckTreeViewBoxUC).ToString();

        private TcMethodClassFunctions_HiVE_CheckBoxControls_Demo_WPF_IsUserControl_CheckTreeViewBoxUC TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        public FooBoxTreeNodeCollection FooBoxTreeNodeItems { get; set; }
        public CntBoxTreeNodeCollection CntBoxTreeNodeItems { get; set; }
        public CheckTreeBox CheckTreeBoxItems { get; set; }

        /// <summary>
        /// Add Node Collection - FooBoxTreeNode
        /// </summary>
        private FooBoxTreeNodeCollection AddNodeCollection_FooBoxTreeNode()
        {
            FooBoxTreeNodeCollection fooBoxTreeNodeCollectionItem = new FooBoxTreeNodeCollection();

            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_CheckBoxControls_Demo_WPF_IsUserControl_CheckTreeViewBoxUC.AddNodeCollection_FooBoxTreeNode).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                #region Define our collection of list items

                int index = 0;

                // define our collection of list items
                var groupedItems = new[] {
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Helium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Helium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Hydrogen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Hydrogen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Oxygen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Oxygen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Argon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Argon.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Iron.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Iron.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Lithium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Lithium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Copper.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Copper.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Gold.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Gold.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Silver.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Silver.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Uranium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Uranium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Plutonium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Plutonium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Americium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Americium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Radon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Radon.ToSpecialDescriptionString()},
                };

                #endregion

                /// Define our collection of list items
                System.Collections.ObjectModel.Collection<FooBoxTreeNode> collectionFooBoxTreeNodeItems =
                    new System.Collections.ObjectModel.Collection<FooBoxTreeNode>();

                foreach (var grp in groupedItems.GroupBy(x => x.Group))
                {
                    FooBoxTreeNode parent = new FooBoxTreeNode(grp.Key);
                    parent.Text = grp.Key;

                    foreach (var item in grp)
                    {
                        FooBoxTreeNode child = parent.Nodes.Add(item.Display);
                        child.Text = item.Display;
                        child.ToolTip = item.ToolTip;
                    }

                    collectionFooBoxTreeNodeItems.Add(parent);
                }

                fooBoxTreeNodeCollectionItem.AddRange(collectionFooBoxTreeNodeItems);
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion

            return fooBoxTreeNodeCollectionItem;
        }

        /// <summary>
        /// Add Node Collection CntBoxTreeNode
        /// </summary>
        private CntBoxTreeNodeCollection AddNodeCollection_CntBoxTreeNode()
        {
            CntBoxTreeNodeCollection cntBoxTreeNodeCollectionItem = new CntBoxTreeNodeCollection();

            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_CheckBoxControls_Demo_WPF_IsUserControl_CheckTreeViewBoxUC.AddNodeCollection_CntBoxTreeNode).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                #region Define our collection of list items

                int index = 0;

                // define our collection of list items
                var groupedItems = new[] {
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Helium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Helium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Hydrogen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Hydrogen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Oxygen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Oxygen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Argon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Argon.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Iron.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Iron.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Lithium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Lithium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Copper.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Copper.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Gold.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Gold.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Silver.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Silver.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Uranium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Uranium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Plutonium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Plutonium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Americium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Americium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Radon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Radon.ToSpecialDescriptionString()},
                };

                #endregion

                /// Define our collection of list items
                System.Collections.ObjectModel.Collection<CntBoxTreeNode> collectionCntBoxTreeNodeItems =
                    new System.Collections.ObjectModel.Collection<CntBoxTreeNode>();

                foreach (var grp in groupedItems.GroupBy(x => x.Group))
                {
                    CntBoxTreeNode parent = new CntBoxTreeNode(grp.Key);
                    parent.Text = grp.Key;

                    foreach (var item in grp)
                    {
                        CntBoxTreeNode child = parent.Nodes.Add(item.Display);
                        child.Text = item.Display;
                        child.ToolTip = item.ToolTip;
                    }

                    collectionCntBoxTreeNodeItems.Add(parent);
                }

                cntBoxTreeNodeCollectionItem.AddRange(collectionCntBoxTreeNodeItems);
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion

            return cntBoxTreeNodeCollectionItem;
        }

        /// <summary>
        /// Add Node Collection CheckTreeBox
        /// </summary>
        private CheckTreeBox AddNodeCollection_CheckTreeBox()
        {
            CheckTreeBox checkTreeBoxItem = new CheckTreeBox();

            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_CheckBoxControls_Demo_WPF_IsUserControl_CheckTreeViewBoxUC.AddNodeCollection_CntBoxTreeNode).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                #region Define our collection of list items

                int index = 0;

                // define our collection of list items
                var groupedItems = new[] {
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Helium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Helium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Hydrogen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Hydrogen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Oxygen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Oxygen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Argon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Argon.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Iron.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Iron.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Lithium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Lithium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Copper.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Copper.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Gold.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Gold.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Silver.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Silver.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Uranium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Uranium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Plutonium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Plutonium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Americium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Americium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Radon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Radon.ToSpecialDescriptionString()},
                };

                #endregion

                /// Define our collection of list items
                System.Collections.ObjectModel.Collection<CntBoxTreeNode> collectionCntBoxTreeNodeItems =
                    new System.Collections.ObjectModel.Collection<CntBoxTreeNode>();

                foreach (var grp in groupedItems.GroupBy(x => x.Group))
                {
                    CntBoxTreeNode parent = new CntBoxTreeNode(grp.Key);
                    parent.Text = grp.Key;

                    foreach (var item in grp)
                    {
                        CntBoxTreeNode child = parent.Nodes.Add(item.Display);
                        child.Text = item.Display;
                        child.ToolTip = item.ToolTip;
                    }

                    collectionCntBoxTreeNodeItems.Add(parent);
                }

                /// Define our collection of list items
                CntBoxTreeNodeCollection collectionComboTreeNodeItemsBox =
                    new CntBoxTreeNodeCollection();

                collectionComboTreeNodeItemsBox.AddRange(collectionCntBoxTreeNodeItems);

                checkTreeBoxItem.Nodes = collectionComboTreeNodeItemsBox;
                checkTreeBoxItem.Initialize();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion

            return checkTreeBoxItem;
        }

        private void buttonSelectAll_CntBoxTreeNode_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                CntBoxTreeNodeItems =
                    SpecialModels.BindCntBoxTreeNodeCollection_IsChecked(
                        CntBoxTreeNodeItems,
                        true);

                treeViewCheckCntBoxTreeNode.ItemsSource = null;
                treeViewCheckCntBoxTreeNode.ItemsSource = CntBoxTreeNodeItems;
            }
            catch { }
        }

        private void buttonUndo_CntBoxTreeNode_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                CntBoxTreeNodeItems =
                    SpecialModels.BindCntBoxTreeNodeCollection_IsChecked(
                        CntBoxTreeNodeItems,
                        false);

                treeViewCheckCntBoxTreeNode.ItemsSource = null;
                treeViewCheckCntBoxTreeNode.ItemsSource = CntBoxTreeNodeItems;
            }
            catch { }
        }

        private void checkTreeViewBoxUC_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_CheckBoxControls_Demo_WPF_IsUserControl_CheckTreeViewBoxUC.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion                
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
