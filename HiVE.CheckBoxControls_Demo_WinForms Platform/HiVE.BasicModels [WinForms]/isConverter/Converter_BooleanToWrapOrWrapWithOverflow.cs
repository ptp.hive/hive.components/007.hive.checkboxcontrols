﻿using System;
using System.Windows;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_BooleanToWrapOrWrapWithOverflow : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            TextWrapping returnValue = TextWrapping.WrapWithOverflow;

            try
            {
                if (value != null)
                {
                    if ((bool)value == true)
                    { returnValue = TextWrapping.Wrap; }
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from Visibility back to IsChecked");
        }
    }
}
