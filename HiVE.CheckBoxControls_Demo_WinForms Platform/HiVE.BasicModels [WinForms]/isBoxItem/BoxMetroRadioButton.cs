﻿using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;

namespace HiVE.BasicModels.isBoxItem
{
    public class BoxMetroRadioButton
    {
        public string Content { get; set; }
        public MethodGroupName GroupName { get; set; }
        public string Background { get; set; }

        public BoxMetroRadioButton() { }

        public BoxMetroRadioButton(
            string content,
            MethodGroupName groupName,
            string background)
        {
            this.Content = content;
            this.GroupName = groupName;
            this.Background = background;
        }
    }
}
