﻿using System.ComponentModel;

namespace HiVE.BasicModels.isBoxItem
{
    /// <summary>
    /// BoxSpecialMethods for this [HiVE.CheckBoxControls] HiVE project.
    /// </summary>
    public static class BoxSpecialMethods
    {
        /// <summary>
        /// Get Special ProductInformation
        /// </summary>
        public enum MethodProductInformation
        {
            [SpecialDescription("HiVE.CheckBoxControls")]
            [Description("HiVE.CheckBoxControls")]
            ProductName,
            [SpecialDescription("http://www.HiVE.CheckBoxControls.com/")]
            [Description("www.HiVE.CheckBoxControls.com")]
            ProductWebSite,
            [SpecialDescription("http://HiVE.CheckBoxControls@gmail.com/")]
            [Description("HiVE.CheckBoxControls@gmail.com")]
            ProductEmail,
            [SpecialDescription("http://www.PTP.HiVE.com/HiVE.CheckBoxControls/")]
            [Description("www.PTP.HiVE.com/HiVE.CheckBoxControls")]
            ProductLink,
        }

        /// <summary>
        /// Define our collection of list items details
        /// Used for CheckBoxControls
        /// </summary>
        public enum MethodNodeListItemDetails
        {
            Group,
            Value,
            Display,
            ToolTip
        }

        public enum MethodNodeListItemDetailsGroup
        {
            Gases,
            Metals,
            Radioactive
        }

        public enum MethodNodeListItemDetailsDisplay
        {
            [SpecialDescription("Lighter than air")]
            Helium,
            [SpecialDescription("Explosive")]
            Hydrogen,
            [SpecialDescription("Vital for animal life")]
            Oxygen,
            [SpecialDescription("Inert")]
            Argon,
            [SpecialDescription("Heavy and metallic")]
            Iron,
            [SpecialDescription("Explodes in water")]
            Lithium,
            [SpecialDescription("Good electrical conductor")]
            Copper,
            [SpecialDescription("Precious metal")]
            Gold,
            [SpecialDescription("Anti-bacterial")]
            Silver,
            [SpecialDescription("Used in fission")]
            Uranium,
            [SpecialDescription("Man-made")]
            Plutonium,
            [SpecialDescription("Used in smoke detectors")]
            Americium,
            [SpecialDescription("Radioactive gas")]
            Radon,

            Deuterium,
            Tritium
        }
    }
}
