﻿namespace HiVE.CheckBoxControls_Demo_WinForms.isUserControl
{
    partial class CheckTreeViewBoxUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelCheckTreeView_FooBoxTreeNode = new System.Windows.Forms.Label();
            this.labelCheckTreeView_CntBoxTreeNode = new System.Windows.Forms.Label();
            this.treeViewCheckFooBoxTreeNode = new System.Windows.Forms.TreeView();
            this.treeViewCheckCntBoxTreeNode = new System.Windows.Forms.TreeView();
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode = new System.Windows.Forms.TableLayoutPanel();
            this.buttonUndo_FooBoxTreeNode = new System.Windows.Forms.Button();
            this.buttonSelectAll_FooBoxTreeNode = new System.Windows.Forms.Button();
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode = new System.Windows.Forms.TableLayoutPanel();
            this.buttonSelectAll_CntBoxTreeNode = new System.Windows.Forms.Button();
            this.buttonUndo_CntBoxTreeNode = new System.Windows.Forms.Button();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.SuspendLayout();
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Controls.Add(this.labelCheckTreeView_FooBoxTreeNode, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelCheckTreeView_CntBoxTreeNode, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.treeViewCheckFooBoxTreeNode, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.treeViewCheckCntBoxTreeNode, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelCheckTreeView_FooBoxTreeNode, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelCheckTreeView_CntBoxTreeNode, 1, 2);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 3;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(540, 480);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // labelCheckTreeView_FooBoxTreeNode
            // 
            this.labelCheckTreeView_FooBoxTreeNode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCheckTreeView_FooBoxTreeNode.AutoSize = true;
            this.labelCheckTreeView_FooBoxTreeNode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelCheckTreeView_FooBoxTreeNode.Location = new System.Drawing.Point(16, 16);
            this.labelCheckTreeView_FooBoxTreeNode.Margin = new System.Windows.Forms.Padding(16);
            this.labelCheckTreeView_FooBoxTreeNode.Name = "labelCheckTreeView_FooBoxTreeNode";
            this.labelCheckTreeView_FooBoxTreeNode.Size = new System.Drawing.Size(238, 13);
            this.labelCheckTreeView_FooBoxTreeNode.TabIndex = 0;
            this.labelCheckTreeView_FooBoxTreeNode.Text = "CheckTreeView - FooBoxTreeNode";
            this.labelCheckTreeView_FooBoxTreeNode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCheckTreeView_CntBoxTreeNode
            // 
            this.labelCheckTreeView_CntBoxTreeNode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCheckTreeView_CntBoxTreeNode.AutoSize = true;
            this.labelCheckTreeView_CntBoxTreeNode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelCheckTreeView_CntBoxTreeNode.Location = new System.Drawing.Point(286, 16);
            this.labelCheckTreeView_CntBoxTreeNode.Margin = new System.Windows.Forms.Padding(16);
            this.labelCheckTreeView_CntBoxTreeNode.Name = "labelCheckTreeView_CntBoxTreeNode";
            this.labelCheckTreeView_CntBoxTreeNode.Size = new System.Drawing.Size(238, 13);
            this.labelCheckTreeView_CntBoxTreeNode.TabIndex = 1;
            this.labelCheckTreeView_CntBoxTreeNode.Text = "CheckTreeView - CntBoxTreeNode";
            this.labelCheckTreeView_CntBoxTreeNode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // treeViewCheckFooBoxTreeNode
            // 
            this.treeViewCheckFooBoxTreeNode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewCheckFooBoxTreeNode.Location = new System.Drawing.Point(3, 48);
            this.treeViewCheckFooBoxTreeNode.Name = "treeViewCheckFooBoxTreeNode";
            this.treeViewCheckFooBoxTreeNode.Size = new System.Drawing.Size(264, 380);
            this.treeViewCheckFooBoxTreeNode.TabIndex = 2;
            // 
            // treeViewCheckCntBoxTreeNode
            // 
            this.treeViewCheckCntBoxTreeNode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewCheckCntBoxTreeNode.Location = new System.Drawing.Point(273, 48);
            this.treeViewCheckCntBoxTreeNode.Name = "treeViewCheckCntBoxTreeNode";
            this.treeViewCheckCntBoxTreeNode.Size = new System.Drawing.Size(264, 380);
            this.treeViewCheckCntBoxTreeNode.TabIndex = 3;
            // 
            // tableLayoutPanelCheckTreeView_FooBoxTreeNode
            // 
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.AutoSize = true;
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.ColumnCount = 4;
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Controls.Add(this.buttonUndo_FooBoxTreeNode, 2, 0);
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Controls.Add(this.buttonSelectAll_FooBoxTreeNode, 1, 0);
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Location = new System.Drawing.Point(10, 441);
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Name = "tableLayoutPanelCheckTreeView_FooBoxTreeNode";
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.RowCount = 1;
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.Size = new System.Drawing.Size(250, 29);
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.TabIndex = 6;
            // 
            // buttonUndo_FooBoxTreeNode
            // 
            this.buttonUndo_FooBoxTreeNode.AutoSize = true;
            this.buttonUndo_FooBoxTreeNode.Location = new System.Drawing.Point(128, 3);
            this.buttonUndo_FooBoxTreeNode.MinimumSize = new System.Drawing.Size(75, 23);
            this.buttonUndo_FooBoxTreeNode.Name = "buttonUndo_FooBoxTreeNode";
            this.buttonUndo_FooBoxTreeNode.Size = new System.Drawing.Size(75, 23);
            this.buttonUndo_FooBoxTreeNode.TabIndex = 1;
            this.buttonUndo_FooBoxTreeNode.Text = "Uncheck All";
            this.buttonUndo_FooBoxTreeNode.UseVisualStyleBackColor = true;
            this.buttonUndo_FooBoxTreeNode.Click += new System.EventHandler(this.buttonUndo_FooBoxTreeNode_Click);
            // 
            // buttonSelectAll_FooBoxTreeNode
            // 
            this.buttonSelectAll_FooBoxTreeNode.AutoSize = true;
            this.buttonSelectAll_FooBoxTreeNode.Location = new System.Drawing.Point(47, 3);
            this.buttonSelectAll_FooBoxTreeNode.MinimumSize = new System.Drawing.Size(75, 23);
            this.buttonSelectAll_FooBoxTreeNode.Name = "buttonSelectAll_FooBoxTreeNode";
            this.buttonSelectAll_FooBoxTreeNode.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectAll_FooBoxTreeNode.TabIndex = 0;
            this.buttonSelectAll_FooBoxTreeNode.Text = "Check All";
            this.buttonSelectAll_FooBoxTreeNode.UseVisualStyleBackColor = true;
            this.buttonSelectAll_FooBoxTreeNode.Click += new System.EventHandler(this.buttonSelectAll_FooBoxTreeNode_Click);
            // 
            // tableLayoutPanelCheckTreeView_CntBoxTreeNode
            // 
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.AutoSize = true;
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.ColumnCount = 4;
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Controls.Add(this.buttonUndo_CntBoxTreeNode, 2, 0);
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Controls.Add(this.buttonSelectAll_CntBoxTreeNode, 1, 0);
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Location = new System.Drawing.Point(280, 441);
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Name = "tableLayoutPanelCheckTreeView_CntBoxTreeNode";
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.RowCount = 1;
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.Size = new System.Drawing.Size(250, 29);
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.TabIndex = 7;
            // 
            // buttonSelectAll_CntBoxTreeNode
            // 
            this.buttonSelectAll_CntBoxTreeNode.AutoSize = true;
            this.buttonSelectAll_CntBoxTreeNode.Location = new System.Drawing.Point(47, 3);
            this.buttonSelectAll_CntBoxTreeNode.MinimumSize = new System.Drawing.Size(75, 23);
            this.buttonSelectAll_CntBoxTreeNode.Name = "buttonSelectAll_CntBoxTreeNode";
            this.buttonSelectAll_CntBoxTreeNode.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectAll_CntBoxTreeNode.TabIndex = 0;
            this.buttonSelectAll_CntBoxTreeNode.Text = "Check All";
            this.buttonSelectAll_CntBoxTreeNode.UseVisualStyleBackColor = true;
            this.buttonSelectAll_CntBoxTreeNode.Click += new System.EventHandler(this.buttonSelectAll_CntBoxTreeNode_Click);
            // 
            // buttonUndo_CntBoxTreeNode
            // 
            this.buttonUndo_CntBoxTreeNode.AutoSize = true;
            this.buttonUndo_CntBoxTreeNode.Location = new System.Drawing.Point(128, 3);
            this.buttonUndo_CntBoxTreeNode.MinimumSize = new System.Drawing.Size(75, 23);
            this.buttonUndo_CntBoxTreeNode.Name = "buttonUndo_CntBoxTreeNode";
            this.buttonUndo_CntBoxTreeNode.Size = new System.Drawing.Size(75, 23);
            this.buttonUndo_CntBoxTreeNode.TabIndex = 1;
            this.buttonUndo_CntBoxTreeNode.Text = "Uncheck All";
            this.buttonUndo_CntBoxTreeNode.UseVisualStyleBackColor = true;
            this.buttonUndo_CntBoxTreeNode.Click += new System.EventHandler(this.buttonUndo_CntBoxTreeNode_Click);
            // 
            // CheckTreeViewBoxUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "CheckTreeViewBoxUC";
            this.Size = new System.Drawing.Size(540, 480);
            this.Load += new System.EventHandler(this.checkTreeViewBoxUC_Loaded);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.ResumeLayout(false);
            this.tableLayoutPanelCheckTreeView_FooBoxTreeNode.PerformLayout();
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.ResumeLayout(false);
            this.tableLayoutPanelCheckTreeView_CntBoxTreeNode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Label labelCheckTreeView_FooBoxTreeNode;
        private System.Windows.Forms.Label labelCheckTreeView_CntBoxTreeNode;
        private System.Windows.Forms.TreeView treeViewCheckFooBoxTreeNode;
        private System.Windows.Forms.TreeView treeViewCheckCntBoxTreeNode;
        private System.Windows.Forms.Button buttonSelectAll_FooBoxTreeNode;
        private System.Windows.Forms.Button buttonUndo_FooBoxTreeNode;
        private System.Windows.Forms.Button buttonSelectAll_CntBoxTreeNode;
        private System.Windows.Forms.Button buttonUndo_CntBoxTreeNode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCheckTreeView_FooBoxTreeNode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCheckTreeView_CntBoxTreeNode;
    }
}
